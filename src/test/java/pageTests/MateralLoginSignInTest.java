package pageTests;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.HomePage;
import pageObjects.SignInPage;
import setup.SetupAndroidTest;

public class MateralLoginSignInTest extends SetupAndroidTest {

	private static final String ANDROID_DEVICE_NAME = "Android Emulator";
	private static final String MATERIAL_LOGIN_APK = "/MaterialLoginExample.apk";
	private static final String APPIUM_AUTOMATION_NAME = "UiAutomator2";
	private static final int NEW_COMMAND_TIMEOUT = 60;

	@DataProvider(name = "testDataForSignInTest")
	public Object[][] getDataForSignInTest() {
		return new Object[][] { { "abcd@gmail.com", "abcdefghi" },};
	}

	@DataProvider(name = "testDataForSignInInvalidTest")
	public Object[][] getDataForSignInInvalidTest() {
		return new Object[][] { { "a", "abcdefghi" },};
	}
	
	@BeforeTest
	public void setupDriver() throws MalformedURLException {
		driver = setupDriver(ANDROID_DEVICE_NAME, MATERIAL_LOGIN_APK, APPIUM_AUTOMATION_NAME, NEW_COMMAND_TIMEOUT);
	}

	@Test(enabled = true, groups = { "signin"}, dataProvider = "testDataForSignInTest")
	public void signInTest(String email, String password) throws InterruptedException {

		SignInPage signInPage = new SignInPage(driver);
		HomePage homePage = new HomePage(driver);

		signInPage.enterUserName(email);
		signInPage.enterPassword(password);
		signInPage.clickSubmitButton();
		Assert.assertTrue(homePage.isCorrectPage());
	}
	
	@Test(enabled = true, groups = { "signin"}, dataProvider = "testDataForSignInInvalidTest")
	public void SignInInvalidTest(String email, String password) throws InterruptedException {

		SignInPage signInPage = new SignInPage(driver);
		signInPage.enterUserName(email);
		signInPage.enterPassword(password);
		signInPage.clickSubmitButton();
		Assert.assertTrue(signInPage.isFailMessageVisible());
	}
	
	
	
	@Test
	public void checkComponentVisiblity() {
		SignInPage signInPage = new SignInPage(driver);
		Assert.assertTrue(signInPage.isEmailComponentVisible());
		Assert.assertTrue(signInPage.isPasswordComponentVisible());
	}

}
