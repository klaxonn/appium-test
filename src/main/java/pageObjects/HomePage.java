package pageObjects;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class HomePage extends PageObject {
	
	@AndroidFindBy(id = "action_bar")
	private MobileElement actionBar;

	public HomePage(AndroidDriver<MobileElement> driver) {
		super(driver);
	}
	
	public boolean isCorrectPage() {
		return actionBar.isDisplayed();
	}

}
